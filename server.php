<?php

use vakata\websocket\Server;

require_once "vendor/autoload.php";

/**
 * instancia del servidor websocket
 */
$server = new Server('ws://127.0.0.1:4200');

/**
 * este evento se produce cuando un cliente envíe un mensaje al servidor. en este caso el manejador reenviará el mensaje
 * a todos los clientes conectados excepto al que remitente
 */
$server->onMessage(function ($sender, $message, $server) {
    foreach ($server->getClients() as $client) {
        if ((int)$sender['socket'] !== (int)$client['socket']) {
            $server->send($client['socket'], $message);
        }
    }
});

/**
 * este evento se produce cuando cuando un cliente se conecta al servidor
 */
$server->onConnect(function ($user){
    echo "nuevo usuario conectado: " . $user['socket'] . "\n";
});

/**
 * este método pone al servidor en ejecución en un bucle infinito
 */
$server->run();
