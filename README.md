# Ejemplo de implementación de WebSocket #

Esta es una sencilla implementación de un chat en tiempo real utilizando web socket.
La implementación del servidor es cortesía de [vakata](https://github.com/vakata/websocket).

### ¿Cómo funciona? ###

* Descarga el repositorio en la raíz de tu servidor por ejemplo en
`C:\xampp\htdocs\websocket-example`
* Verifica que puedes acceder a la página introduciendo esta url en tu navegador favorito `localhost/websocket-example`
* Si todo ha ido bien, ahora deberías poner en ejecución el servidor, para eso sigue los siguientes pasos
* Abre una ventana de comandos y cambia al directorio del ejemplo ejecutando el comando `cd C:\xampp\htdocs\websocket-example
`
* Una vez ahí ejecuta el comando `php -q server.php`, esto pondrá en ejecución al servidor.
* Recarga la página del navegador y ahora debe estar conectado.
* Abra otra ventana del navegador e intente enviar un mensaje.

### Consideraciones ###

El servidor abrirá el puerto 4200 en tu ordenador, así que verifica que este puerto no esté en uso, por ejemplo por una aplicación de Angular, de ser así cambie a otro puerto por ejemplo el 8080
